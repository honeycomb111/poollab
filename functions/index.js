const functions = require('firebase-functions');

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//

const stripe = require('stripe')(functions.config().stripe.token)
const admin = require('firebase-admin');
const cors = require('cors')({origin: true});

admin.initializeApp(functions.config().firebase);

 exports.createStripeCustomer = functions.auth.user().onCreate((event) => {
   const data = event;
  return stripe.customers.create({
    email: data.email
  }).then((customer) => {
    return admin.database().ref(`/Customers/${data.uid}/Payment_sources/Stripe_customer_id`).set(customer.id);
  });
});

exports.listCustomerCards = functions.https.onRequest((req, res) => {
  return stripe.customers.listCards(req.query.customerId).then((cards)=> {
    return cors(req, res, () => {

    res.status(200).send(cards.data);
    return cards
    });
  }, (error) => {
    res.status(500).send(error);
    return error
  })
});

exports.updateStripeCustomer = functions.database.ref('/Customers/{userId}').onUpdate((snapshot, context)  => {
  const data = snapshot.after.val();
    return stripe.customers.createSource(data.payment_sources.customer_id, {
      source: data.payment_sources.source
    }).then((customer) => {
      return customer
    }, (error)=>{
      return error
    });
});


exports.saveCharge = functions.https.onRequest((req, res) => {
  return stripe.charges.create({
    amount: req.body.totalCharges,
    currency: req.body.currency,
    customer: req.body.customer_id
  }).then((charge) => {
      charge.createdAt = req.body.createdAt;
      charge.userId = req.body.userId;
      charge.additionalCharges = req.body.additionalCharges;
      charge.totalCharges = req.body.totalCharges;
      charge.totalCharges = req.body.totalCharges;
      charge.fname = req.body.fname;
      charge.lname = req.body.lname;
      charge.service_month = req.body.service_month;
      charge.email = req.body.email;

    admin.database().ref(`/Customers/Charges/{charge.createdAt}`).push(charge);
    return cors(req, res, () => {
      res.status(200).send(charge);
      return charge
    });
  }, (error) => {
    cors(req, res, () => {
      res.status(500).send(error);
      return error
    })
  })
});
exports.addPaymentSource = functions.database.ref('/Customers/{userId}/payment_sources/{pushId}/token').onWrite((event) => {
  const source = event.data.val();
  if (source === null) return null;
  return admin.database().ref(`/Customers/${event.params.userId}/customer_id`).once('value').then((snapshot) => {
    return snapshot.val();
  }).then((customer) => {
    return stripe.customers.createSource(customer, {source});
  }).then((response) => {
    return event.data.adminRef.parent.set(response);
  }, (error) => {
    return event.data.adminRef.parent.child('error').set(userFacingMessage(error));
  }).then(() => {
    return reportError(error, {user: event.params.userId});
  });
});

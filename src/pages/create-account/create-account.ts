import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SetupCardPage } from '../setup-card/setup-card';
import { LoginPage } from '../login/login';
import { UserService } from '../../providers/user-service';
import {AngularFireDatabase} from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase';

@Component({
  selector: 'page-create-account',
  templateUrl: 'create-account.html'
})


export class CreateAccountPage {

  confirmPassword: string;
  // user: UserObject;
  constructor(public navCtrl: NavController,
              private afAuth: AngularFireAuth,
              public user: UserService,
              private angularDb: AngularFireDatabase
              ) {
  }

  goToLogin(params){
    if (!params) params = {};
    this.navCtrl.push(LoginPage);
  }


  goToSignupBank(params){
    if (!params) params = {};

    // Make all fields required

    if (this.user.fname == '' || this.user.fname == null) {
      alert ("Please enter your first name");
      return
    }

    if (this.user.lname == '' || this.user.lname == null) {
      alert ("Please enter your last name");
      return
    }

    if (this.user.phone == '' || this.user.phone == null) {
      alert ("Please enter your phone number");
      return
    }

    if (this.user.email == '' || this.user.email == null) {
      alert ("Please enter email");
      return
    }

    if (this.user.password == '' || this.user.password == null) {
      alert ("Please enter password");
      return
    }

    if (this.user.password != this.confirmPassword ) {
      alert ("Password does not match");
      return
    }

    // Login User in Firebase
    this.afAuth
    .auth
    .createUserWithEmailAndPassword(this.user.email, this.user.password)
    .then(userDetails => {
      console.log('Account Created Successfully', JSON.stringify(userDetails));
      // Store account information in firebase

        this.angularDb.object('/Customers/' + userDetails.uid + '/Details').update({
        uid: userDetails.uid,
        fname: this.user.fname,
        lname: this.user.lname,
        phone: this.user.phone,
        email: this.user.email,
        cardsetup: 0,
        monthlyrate: this.user.monthlyrate
       }).then((success)=> {
         this.navCtrl.push(SetupCardPage);
       }).catch ((error) => {
         alert ("Error Storing Customer Profile")
       })
    })
    .catch(err => {
      alert(err.message);
      console.log('Error creating account',err.message);
    });
  }

}

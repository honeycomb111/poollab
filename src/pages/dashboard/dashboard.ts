import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { UserService } from '../../providers/user-service';
import { SetupCardPage } from '../setup-card/setup-card';
import { MakePaymentPage } from '../make-payment/make-payment';
import { PaymentHistoryPage } from '../payment-history/payment-history'
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html'
})
export class DashboardPage {

  constructor(public navCtrl: NavController,
              public user: UserService) {

  }
  gotocard()
  {
    this.navCtrl.push(SetupCardPage);
  }
  monthlyPayment()
  {
    this.navCtrl.push(MakePaymentPage);
  }
  paymentHistory()
  {
    this.navCtrl.push(PaymentHistoryPage);
  }
}

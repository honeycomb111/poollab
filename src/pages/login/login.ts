import { DashboardPage } from './../dashboard/dashboard';
import { UserService } from '../../providers/user-service';
import { Component } from '@angular/core';
import { NavController} from 'ionic-angular';
import { CreateAccountPage } from '../create-account/create-account';

import { AngularFireAuth } from 'angularfire2/auth';
import {AngularFireDatabase } from 'angularfire2/database';

import * as firebase from 'firebase/app';
// import { Observable } from 'rxjs/Observable';
import { Storage } from '@ionic/storage';
import { Observable } from 'rxjs/Observable';


@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  email: string;
  password: string;
  // bankSetup: string;

  constructor(public navCtrl: NavController,
              private afAuth: AngularFireAuth,
              private afDB: AngularFireDatabase,
              private storage: Storage,
              public user: UserService
            ) {
              storage.get('user')
                .then((data) => {
                  this.email = data;
                }).catch((err) => {
                  console.log("error reading user name from storage");
                });

              storage.get('password')
                .then((data) => {
                  this.password = data;
                }).catch((err) => {
                  console.log("error reading user name from storage");
                });
  }

  goToLogin(params){
    if (!params) params = {};

    if (this.email == '' || this.email == null) {
      alert ("Please enter your email");
      return
    }

    if (this.password =='' || this.password == null) {
      alert ("Please enter password");
      return
    }

    // Login User in Firebase
    this.afAuth
    .auth
    .signInWithEmailAndPassword(this.email, this.password)
    .then(userDetails => {
      console.log('Login Successfull');

      const userRef$: Observable <any> = this.afDB.object('/Customers/'+userDetails.uid).valueChanges();

      userRef$.subscribe( afUser =>  {
          this.user.uid = afUser.uid;
          // this.user.paymentacct = afUser.paymentacct;
          this.user.fname = afUser.fname;
          this.user.lname = afUser.lname;
          this.user.phone = afUser.phone;
          this.user.email = afUser.email;
          this.user.cardsetup = afUser.banksetup;
          this.user.monthlyrate = afUser.rate;
          console.log("User found", JSON.stringify(this.user));
          this.storage.set('user',this.email);
          this.storage.set('password',this.password);

          this.navCtrl.push(DashboardPage);
       });


    })
    .catch(err => {
      alert("incorrect login");
      console.log('Incorrect Login:',err.message);
    });
  }

  goToCreateAccount(params){
    if (!params) params = {};
    this.navCtrl.push(CreateAccountPage);
  }

  // goToForgotPassword(params){
  //   if (!params) params = {};
  //   this.navCtrl.push(ForgotPasswordPage);
  // }

  // goToTabsController(params){
  //   if (!params) params = {};
  //   console.log('check bank setup ->', this.user.banksetup);
  //   if (this.user.banksetup == "complete") {
  //     console.log('Bank Setup is complete')
  //     this.navCtrl.push(TabsControllerPage);
  //   } else {
  //     this.navCtrl.push(SignupBankPage, {
  //       'bankMode': 'incomplete'
  //     });
  //   }
  // }
}


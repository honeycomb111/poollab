import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import * as firebase from 'firebase';
import {AngularFireDatabase} from 'angularfire2/database';
import moment from 'moment'
import { Stripe } from '@ionic-native/stripe';
import {LoginPage} from '../login/login'
/**
 * Generated class for the SetupCardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-setup-card',
  templateUrl: 'setup-card.html',
})
export class SetupCardPage {
  user:any;
  submitted: boolean = false;
  card: any = {};

  constructor(public navCtrl:NavController, public angularDb:AngularFireDatabase,
              public navParams:NavParams,
              private stripe: Stripe, private loadingCtrl: LoadingController) {
    this.stripe.setPublishableKey('pk_test_KvDv3BlstkOsQCZHLc9ndzwY')
    this.user = {}
  }

  async ionViewDidLoad() {
   this.angularDb.object('/Customers/' + firebase.auth().currentUser.uid).valueChanges().subscribe((user)=> {
     this.user = user;
     this.card.email = this.user.Details.email;
   });
  }

  savecard(form: any) {
    let loading = this.loadingCtrl.create({
      content: "Saving card..."});
    loading.present();
    this.submitted = true;
    if(form.invalid){
      loading.dismiss()
      return;
    }
    this.card.expMonth = moment(this.card.date).format('MM');
    this.card.expYear = moment(this.card.date).format('YYYY');
    this.stripe.createCardToken(this.card).then((token) => {
      this.card.source = token;
      this.angularDb.object('/Customers/' + firebase.auth().currentUser.uid).update({
        Details:{
          cardsetup: 1
        },
        Payment_sources: {
          Stripe_customer_id: this.user.Payment_sources.Stripe_customer_id,
          Stripe_Token: this.card.source.id
        }
      }).then((res) => {
        loading.dismiss()
        this.navCtrl.push(LoginPage)

      }, (error) => {
        loading.dismiss()
        console.log("=========>", error)
        alert(JSON.stringify(error))
      });
    })



  }
  handleCardChange() {
    let myStr = this.card.number.split(" ").join("");
    this.card.number = myStr.match(new RegExp('.{1,4}$|.{1,4}', 'g')).join(" ");
  }
}

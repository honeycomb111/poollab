import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import {AngularFireDatabase} from 'angularfire2/database';
import * as firebase from 'firebase';
import {DashboardPage} from '../dashboard/dashboard'
/**
 * Generated class for the PaymentHistoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-payment-history',
  templateUrl: 'payment-history.html',
})
export class PaymentHistoryPage {
  charges: any = [];

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public angularDb:AngularFireDatabase,
              private loadingCtrl: LoadingController) {
  }

  ionViewDidLoad() {
    let loading = this.loadingCtrl.create();
    loading.present();
    this.angularDb.list('/Customers/'+ firebase.auth().currentUser.uid + '/Charges').valueChanges().subscribe((res)=> {
      this.charges = res;
      loading.dismiss()

    });
  }
  openDashboard(){
    this.navCtrl.setRoot(DashboardPage)
  }
}

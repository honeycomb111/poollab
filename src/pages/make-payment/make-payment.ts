import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import {AngularFireDatabase} from 'angularfire2/database';
import * as firebase from 'firebase';
import {SetupCardPage} from '../setup-card/setup-card'
import {HttpClient, HttpHeaders} from '@angular/common/http'
import moment from 'moment';
import {PaymentHistoryPage} from '../payment-history/payment-history'
/**
 * Generated class for the MakePaymentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-make-payment',
  templateUrl: 'make-payment.html',
})
export class MakePaymentPage {
  user:any = {};
  lastCharge: any = {};
  card: any = {};
  exp_date: any;
  error: string;
  serviceMonth: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public angularDb:AngularFireDatabase,
              private http: HttpClient,
              private loadingCtrl: LoadingController) {
  }

  ionViewDidLoad() {
    let loading = this.loadingCtrl.create();
    loading.present();
    this.angularDb.object('/Customers/' + firebase.auth().currentUser.uid).valueChanges().subscribe((user)=> {
      this.user = user;
      this.user.additionalCharges = Number(this.user.additionalCharges) || 0;
      this.user.totalCharges = Number(this.user.monthlyrate) + this.user.additionalCharges;
      this.http.get('https://us-central1-pool-lab.cloudfunctions.net/listCustomerCards?customerId=' + this.user.payment_sources.customer_id)
        .subscribe(data => {
          loading.dismiss();
          this.card = data[0];
          let date = new Date();
          this.serviceMonth = moment(date).format('MMMM')
        }, (error)=>{
          loading.dismiss()
        })
    });

    this.angularDb.list('/Charges/' + firebase.auth().currentUser.uid + '/Charges', ref => ref.orderByChild("createdAt").limitToLast(1)).valueChanges().subscribe((res)=> {
      var that = this
      Object.keys(res).forEach(function(key) {
        that.lastCharge = res[key];
      });
    });

    console.log('ionViewDidLoad MakePaymentPage');
  }
  processPayment() {
    let exp_month = moment(this.exp_date).format('MM');
    let exp_year = moment(this.exp_date).format('YYYY');
    if (exp_month != this.card.exp_month && exp_year != this.card.exp_year) {
      this.error = "Invalid expiry date!";
      return
    }
    let loading = this.loadingCtrl.create();
    loading.present();

    this.http.post('https://us-central1-pool-lab.cloudfunctions.net/saveCharge', {
      amount: Number(this.user.monthlyrate)*100,
      createdAt: Date.now(),
      currency: 'usd',
      customer_id: this.user.payment_sources.customer_id,
      userId: this.user.uid,
      additionalCharges: this.user.additionalCharges,
      totalCharges: Number(this.user.monthlyrate)*100 + Number(this.user.additionalCharges)*100,
      source: this.user.payment_sources.source,
      fname: this.user.fname,
      lname: this.user.lname,
      service_month: this.serviceMonth,
      email: this.user.email
    })
      .subscribe(data => {
        loading.dismiss();
        this.navCtrl.push(PaymentHistoryPage)
      }, (error)=>{
        loading.dismiss()
      });


  }
  goToSetupCard() {
    this.navCtrl.push(SetupCardPage)
  }

}

import { Injectable } from '@angular/core';

@Injectable()
export class UserService {
    uid: string;
    // paymentacct: string;
    fname: string;
    lname: string; 
    phone: string;
    email: string;
    password: string;
    cardsetup: string;
    monthlyrate: string;

  constructor() {
      
  }

  clear(){
    this.uid = '';
    // this.paymentacct = '';
    this.fname = '';
    this.lname = '';
    this.phone = '';
    this.email = '';
    this.password = '';
    this.cardsetup = '';
    this.monthlyrate = '';
  }
}
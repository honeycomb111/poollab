import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpModule } from '@angular/http';
import {HttpClientModule} from '@angular/common/http';

import { MyApp } from './app.component';
import { DashboardPage } from '../pages/dashboard/dashboard';
import { LoginPage } from '../pages/login/login';
import { SetupCardPage } from '../pages/setup-card/setup-card';

import { AddOnChargesPage } from '../pages/add-on-charges/add-on-charges';
import { CreateAccountPage } from '../pages/create-account/create-account';
import { MakePaymentPage } from '../pages/make-payment/make-payment';
import { PaymentHistoryPage } from '../pages/payment-history/payment-history';
import { UserService } from '../providers/user-service';
import { UtilityService } from '../providers/utility-service';

import { AngularFireModule } from 'angularfire2' ;
import { Stripe } from '@ionic-native/stripe';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { IonicStorageModule } from '@ionic/storage';
import * as firebase from 'firebase/app';
var firebaseConfig = {
    apiKey: "AIzaSyA9YG7ODFVUNRVGI0EoC0uuj9yj3SCjztA",
    authDomain: "pool-lab.firebaseapp.com",
    databaseURL: "https://pool-lab.firebaseio.com",
    projectId: "pool-lab",
    storageBucket: "pool-lab.appspot.com",
    messagingSenderId: "578048153775"
};
firebase.initializeApp(firebaseConfig);
@NgModule({
  declarations: [
    MyApp,
    DashboardPage,
    LoginPage,
    SetupCardPage,
    AddOnChargesPage,
    CreateAccountPage,
    MakePaymentPage,
    PaymentHistoryPage

  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(MyApp),
    HttpModule,
    HttpClientModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    DashboardPage,
    SetupCardPage,
    LoginPage,
    AddOnChargesPage,
    CreateAccountPage,
    MakePaymentPage,
    PaymentHistoryPage

  ],
  providers: [
    StatusBar,
    SplashScreen,
    UserService,
    UtilityService,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    HttpClientModule,
    Stripe
  ]
})
export class AppModule {}
